#!/bin/bash

# Bootstrap saltstack on the master node
sudo curl -L https://bootstrap.saltstack.com | sudo bash -s -- -M -P stable
# Configure the minion on the master node
sudo su -c "echo 'master: salt' >> /etc/salt/minion"
sudo service salt-minion restart
sleep 1 # Make sure that the minion submits their key before accepting it
sudo salt-key -A -y
# Copy all states and configuration to appropriate directories
sudo cp -r pillar salt /srv/
sudo cp cloud.profiles cloud.providers gce.pem /etc/salt
sudo apt-get -y install salt-cloud
# Work around a bug in the version of libcloud contained in the Ubuntu repository
sudo pip install -U apache-libcloud
# Allow the master to access the minions that are created
sudo su ubuntu -c "ssh-keygen -f /home/ubuntu/.ssh/id_rsa -t rsa -N ''"
sudo su -c 'echo "  metadata: '\''{\"sshKeys\": \"ubuntu:$(sudo cat /home/ubuntu/.ssh/id_rsa.pub)\"}'\''" >> /etc/salt/cloud.providers'
# Create the api and nginx minions
sudo salt-cloud -p api api1
sudo salt-cloud -p nginx nginx1
# Configure the minions
sudo salt \* state.highstate
