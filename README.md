## Setup
The first thing that you must do is to obtain a JSON formatted credentials file for a service account on Google Compute engine. Once you obtain that file, place it in this directory as gce.pem.

The next step is to [install the Google Cloud SDK](https://cloud.google.com/sdk/), followed by installing the python requirements in requirements.txt using your favorite virtualenv tools.

After this setup is complete simply execute `sh bootstrap.sh` and it will bootstrap a hello world API on Google Compute using SaltStack. There will be an Nginx host and an API host. By modifying the script `setup.sh` to create more API instances you can have Nginx load-balance across the instances.

If more API instances are launched by saltstack then simply running a highstate on the nginx node will update the list of hosts for balancing across.
