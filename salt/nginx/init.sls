{% set api_host_list = salt['mine.get']('role:api', 'network.ip_addrs', expr_form='grain') %}
{% set uwsgi_port = salt['pillar.get']('api:uwsgi:port', 80) %}

install_nginx:
  pkg.installed:
    - name: nginx
    - refresh: True

nginx_config:
  file.managed:
    - name: /etc/nginx/sites-available/api.conf
    - source: salt://nginx/files/api.conf
    - template: jinja
    - context:
        api_host_list: {{ api_host_list }}
        uwsgi_port: {{ uwsgi_port }}

activate_nginx_config:
  file.symlink:
    - name: /etc/nginx/sites-enabled/api.conf
    - target: /etc/nginx/sites-available/api.conf

remove_default_config:
  file.absent:
    - name: /etc/nginx/sites-enabled/default

nginx_service:
  service.running:
    - name: nginx
    - enable: True
    - watch:
        - file: nginx_config